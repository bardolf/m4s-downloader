# m4s downloader

```shell
cat 1001-1502_IS.mp4 $(ls -vx 1002-1502*.m4s) > audio.m4a
cat 1001-1505_IS.mp4 $(ls -vx 1002-1505*.m4s) > video.mp4

ffprobe audio.m4a 
Input #0, mov,mp4,m4a,3gp,3g2,mj2, from 'audio.m4a':
  Metadata:
    major_brand     : iso6
    minor_version   : 1
    compatible_brands: iso6isomavc1dash
  Duration: 00:11:55.91, start: 0.009333, bitrate: 131 kb/s
    Stream #0:0(und): Audio: aac (LC) (mp4a / 0x6134706D), 48000 Hz, stereo, fltp, 125 kb/s (default)

ffprobe video.mp4 
Input #0, mov,mp4,m4a,3gp,3g2,mj2, from 'video.mp4':
  Metadata:
    major_brand     : iso6
    minor_version   : 1
    compatible_brands: iso6isomavc1dash
  Duration: 00:11:55.92, start: 2.040000, bitrate: 3492 kb/s
    Stream #0:0(und): Video: h264 (Main) (avc1 / 0x31637661), yuv420p, 1280x720 [SAR 1:1 DAR 16:9], 3489 kb/s, 25 fps, 25 tbr, 90k tbn, 50 tbc (default)
```

The offset needs to be fixed (https://superuser.com/questions/982342/in-ffmpeg-how-to-delay-only-the-audio-of-a-mp4-video-without-converting-the-au)

Difference is 2.04 - 0.009333 = 2.030667

```
ffmpeg -i audio.m4a -itsoffset 2.030667 -i video.mp4 -map 1:v -map 0:a  -c copy output.m4v
```