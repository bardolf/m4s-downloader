const puppeteer = require('puppeteer');
const fs = require('fs');
const tmp = require('tmp');

const CHROMIUM_BROWSER_PATH = '/usr/bin/chromium-browser';

async function download(url, dir) {
    const browser = await puppeteer.launch({ headless: false, executablePath: CHROMIUM_BROWSER_PATH });
    const page = await browser.newPage();

    await page.goto(url);

    page.on('response', response => {
        if (response.url().endsWith('m4s') || response.url().endsWith('mp4')) {
            var parts = response.url().split("/");
            var name = parts[parts.length - 1];
            var path = parts[parts.length - 2];
            store(response.buffer(), name, dir.name + "/" + path);
        }
    });
}

function store(bufferPromise, name, path) {
    var filename = path + "_" + name;
    console.log('Writing to a file ' + filename);
    bufferPromise.then(buffer => (
        fs.writeFile(filename, buffer, (err) => {
            if (err) throw err;
            console.log('Wrote the file successfully ' + filename);
        })
    ));
}

var dir = tmp.dirSync();
console.log(dir);
var url = 'https://www.ceskatelevize.cz/ivysilani/12874958681-ms-v-ragby-2019-japonsko/219471291300006-irsko-skotsko';
download(url, dir);
